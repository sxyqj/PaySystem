package com.so206.mapper.ext;

import com.so206.po.SysFunction;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMapperExt {

    List<SysFunction> find_function_with_role(@Param("role_id") Integer role_id,
                                              @Param("is_menu") Integer is_menu,
                                              @Param("parent_id") Integer parent_id);


}
