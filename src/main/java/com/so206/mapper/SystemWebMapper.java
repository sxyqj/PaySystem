package com.so206.mapper;

import com.so206.po.SystemWeb;
import com.so206.po.SystemWebExample;
import com.so206.po.SystemWebWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemWebMapper {
    long countByExample(SystemWebExample example);

    int deleteByExample(SystemWebExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SystemWebWithBLOBs record);

    int insertSelective(SystemWebWithBLOBs record);

    List<SystemWebWithBLOBs> selectByExampleWithBLOBs(SystemWebExample example);

    List<SystemWeb> selectByExample(SystemWebExample example);

    SystemWebWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SystemWebWithBLOBs record, @Param("example") SystemWebExample example);

    int updateByExampleWithBLOBs(@Param("record") SystemWebWithBLOBs record, @Param("example") SystemWebExample example);

    int updateByExample(@Param("record") SystemWeb record, @Param("example") SystemWebExample example);

    int updateByPrimaryKeySelective(SystemWebWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(SystemWebWithBLOBs record);

    int updateByPrimaryKey(SystemWeb record);
}