package com.so206.mapper;

import com.so206.po.SysExtension;
import com.so206.po.SysExtensionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysExtensionMapper {
    long countByExample(SysExtensionExample example);

    int deleteByExample(SysExtensionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysExtension record);

    int insertSelective(SysExtension record);

    List<SysExtension> selectByExample(SysExtensionExample example);

    SysExtension selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysExtension record, @Param("example") SysExtensionExample example);

    int updateByExample(@Param("record") SysExtension record, @Param("example") SysExtensionExample example);

    int updateByPrimaryKeySelective(SysExtension record);

    int updateByPrimaryKey(SysExtension record);
}